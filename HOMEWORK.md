# Multithreading Homework

1. First commit is single thread application.
2. Second commit multithreaded application but inefficient without
StringGenerator timeout.
3. Third commit is working version. Can work with any number of 
decryptors, and all of it will stops correctly, but optimal solution 
one StringGenerator and one decryptor working in parallel. It is more 
effective than single thread variant up to 3-5 times.