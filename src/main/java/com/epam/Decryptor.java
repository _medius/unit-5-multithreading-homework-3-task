package com.epam;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.*;

public class Decryptor implements Callable<String> {

	private static int idSequence = 0;

	{
		id = ++idSequence;
	}

	private final int id;
	private final BlockingQueue<String> queue;
	private final String hashToFind;
	private final CountDownLatch interruptSynchronizer;

	public Decryptor(CountDownLatch interruptSynchronizer, BlockingQueue<String> queue, String hashToFind) {
		this.queue = queue;
		this.hashToFind = hashToFind;
		this.interruptSynchronizer = interruptSynchronizer;
	}

	@SuppressWarnings({"UnstableApiUsage", "deprecation"})
	private String getHash(String toHash) {
		Hasher hasher = Hashing.md5().newHasher();
		hasher.putString(toHash, StandardCharsets.UTF_8);
		return hasher.hash().toString();
	}

	@Override
	public String call() {
		long counter = 0;

		String currentPassword = "";
		String hash = "";

		try {
			while (!hash.equals(hashToFind)) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				currentPassword = queue.take();
				hash = getHash(currentPassword);
				counter++;
				//Console output adds approximately 4-5 minutes
//				System.out.println(this + " processed " + counter + " strings. Queue size is " + queue.size());
			}
		} catch (InterruptedException e) {
			interruptSynchronizer.countDown();
			System.out.println(this + " has interrupted. Last processed string is "
					+ currentPassword + ", " + counter + " passwords processed.");
		}

		return this + " has founded password. PASSWORD IS "
				+ currentPassword + ", " + counter + " passwords processed.";
	}

	@Override
	public String toString() {
		return "Decryptor " + id;
	}
}
