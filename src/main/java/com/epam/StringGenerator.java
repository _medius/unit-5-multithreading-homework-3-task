package com.epam;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class StringGenerator implements Runnable {

	private static final int PUT_TIMEOUT = 1000;

	private final BlockingQueue<String> queue;
	private final String alphabet = "abcdefghijklmnopqrstuvwxyz";
	private final CountDownLatch interruptSynchronizer;
	private StringBuilder stringBuilder = new StringBuilder();
	private List<Integer> sequence = new ArrayList<>();
	

	public StringGenerator(CountDownLatch interruptSynchronizer, BlockingQueue<String> queue) {
		sequence.add(0);
		this.queue = queue;
		this.interruptSynchronizer = interruptSynchronizer;
	}

	private String getNextString() {
		stringBuilder.setLength(0);
		for (int integer : sequence) {
			stringBuilder.append(alphabet.charAt(integer));
		}
		incrementSequence();
		return stringBuilder.toString();
	}

	private void incrementSequence() {
		boolean positionIncremented;
		int position = 0;
		int currentSymbol;

		do {
			positionIncremented = false;

			currentSymbol = sequence.get(position);
			if (++currentSymbol == alphabet.length()) {
				sequence.set(position, 0);
				if (++position == sequence.size()) {
					sequence.add(0);
					break;
				}
				positionIncremented = true;
			} else {
				sequence.set(position, currentSymbol);
			}

		} while (positionIncremented);
	}

	@Override
	public void run() {
		try {
			while (true) {

				for (int i = 0; i < queue.remainingCapacity(); i++) {
					if (Thread.currentThread().isInterrupted()) {
						throw new InterruptedException();
					}
					queue.put(getNextString());
				}

				TimeUnit.MICROSECONDS.sleep(PUT_TIMEOUT);
			}
		} catch (InterruptedException e) {
			interruptSynchronizer.countDown();			
			System.out.println(this + " has stopped.");
		}
	}

	@Override
	public String toString() {
		return "StringGenerator";
	}
}
