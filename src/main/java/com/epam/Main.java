package com.epam;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {

	private static final String HASH_TO_DECRYPT = "ddc4035ff6451f85bb796879e9e5ea49";
	private static final int QUEUE_SIZE = 2000;
	private static final int NUMBER_OF_DECRYPTORS = 1;
	private static final int THREAD_POOL_SIZE = NUMBER_OF_DECRYPTORS + 1;

	private BlockingQueue<String> queue = new ArrayBlockingQueue<>(QUEUE_SIZE);
	private List<Decryptor> decryptors = new ArrayList<>();
	private CountDownLatch interruptSynchronizer = new CountDownLatch(NUMBER_OF_DECRYPTORS);

	private void appStart() throws ExecutionException, InterruptedException {
		ExecutorService service = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

		for (int i = 0; i < NUMBER_OF_DECRYPTORS; i++) {
			decryptors.add(new Decryptor(interruptSynchronizer, queue, HASH_TO_DECRYPT));
		}
		double timeStart = System.currentTimeMillis();
		service.submit(new StringGenerator(interruptSynchronizer, queue));
		String result = service.invokeAny(decryptors);

		service.shutdownNow();
		double timeStop = System.currentTimeMillis();
		double timePassed = (timeStop - timeStart)/60_000;
		service.awaitTermination(300, TimeUnit.MILLISECONDS);
		System.out.printf(result + " Password was found in %.2f minutes.", timePassed);
	}

	public static void main(String[] args) throws ExecutionException, InterruptedException {
		new Main().appStart();
	}
}
